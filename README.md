# AutoHotkey-Scripts
A collection of AutoHotkey scripts I wrote for various tasks.

Everything is licenced under:

[![Licence](https://gitlab.com/Unrepentant-Atheist/AutoHotkey-Scripts/raw/2210288daba2a70e4509e1a527620637bfa9e48e/CC_licence.png)](https://creativecommons.org/licenses/by-nc-sa/4.0/)